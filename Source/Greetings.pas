unit Greetings;

interface

type

  TLanguage = (English, German, Swedish, Norwegian);

  TGreetings = class
  private
    FLanguage: TLanguage;
  public
    procedure SetLanguage(Language: TLanguage);
    function GetHelloWorld: string;
  end;

implementation

{ TGreetings }

function TGreetings.GetHelloWorld: string;
begin
  Result := 'Hello World';
end;

procedure TGreetings.SetLanguage(Language: TLanguage);
begin
  FLanguage := Language;
end;

end.
