unit TestGreetings;

interface

uses
  DUnitX.TestFramework, Greetings;

type
  [TestFixture]
  TTestGreetings = class
  private
    FGreetings: TGreetings;
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    // Sample Methods
    // Simple single Test
    [Test]
    procedure TestHelloWorld;
  end;

implementation

procedure TTestGreetings.Setup;
begin
  FGreetings := TGreetings.Create;
  FGreetings.SetLanguage(TLanguage.English);
end;

procedure TTestGreetings.TearDown;
begin
  FGreetings.Free;
end;

procedure TTestGreetings.TestHelloWorld;
begin
  Assert.AreEqual('Hello World', FGreetings.GetHelloWorld);
end;

initialization
  TDUnitX.RegisterTestFixture(TTestGreetings);

end.
